package com.example.jdbcandmaven.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ConnectWithDatabaseTest.class, CreateTableOrderTest.class, 
						AddNewOrderTest.class, ViewOrdersTest.class,
						DeleteOrderTest.class})
public class RunAllTests {

}
