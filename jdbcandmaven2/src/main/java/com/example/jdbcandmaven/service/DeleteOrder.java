package com.example.jdbcandmaven.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteOrder {
	
	private PreparedStatement deleteOrder;

	private String deleteOrderSql = "DELETE FROM Orders";

	private Connection connection;

	public DeleteOrder () {
		setConnection();
	}
	
	private void setConnection() {
		this.connection = new ConnectWithDatabase().getConnection();
		}
	
	public boolean deleteOrder() {
		boolean isTableDelete = false;
		try{
		deleteOrder = this.connection.prepareStatement(deleteOrderSql);
		int counter = 0;
		counter = deleteOrder.executeUpdate();
		if (counter > 0) isTableDelete = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			return isTableDelete;

		}
	}
}
