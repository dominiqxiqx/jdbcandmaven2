package com.example.jdbcandmaven.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddNewOrder {
	
	private PreparedStatement addNewOrder;
	
	private String addNewOrderSql = "INSERT INTO Orders (id, client, deliveryAddress, items) VALUES (1, '?', '?', '?')";
	
	private Connection connection;
	
	public AddNewOrder() {
		setConnection();
	}
	
	private void setConnection() {
		this.connection = new ConnectWithDatabase().getConnection();
		}
	
	public boolean newOrder() {
		boolean isTableCreate = false;
		try{
		addNewOrder = this.connection.prepareStatement(addNewOrderSql);
		int counter = 0;
		counter = addNewOrder.executeUpdate();
		if (counter > 0) isTableCreate = true;
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			return isTableCreate;
		}
	}
}
