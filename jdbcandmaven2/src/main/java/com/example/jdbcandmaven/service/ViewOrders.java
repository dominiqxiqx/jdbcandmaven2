package com.example.jdbcandmaven.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcandmaven.domain.ClientDetails;
import com.example.jdbcandmaven.domain.Order;
import com.example.jdbcandmaven.domain.OrderItem;

public class ViewOrders {
	
	private PreparedStatement viewOrders;

	private String viewOrdersSql = "SELECT id, client, items FROM Orders";
	
	private Connection connection;
	
	public ViewOrders() {
		setConnection();
	}
	
	private void setConnection() {
		this.connection = new ConnectWithDatabase().getConnection();
		}
	
	public boolean viewOrders() {
		boolean isTableInView = false;
		try {
		viewOrders = this.connection.prepareStatement(viewOrdersSql);
		ResultSet rs = viewOrders.executeQuery();
		
		Order domainOrder = new Order();
		while(rs.next()) {
			
			domainOrder.setId(rs.getLong("id"));
			
			ClientDetails domainClientDetails = new ClientDetails();
			domainClientDetails.setName(rs.getString("client"));
			domainOrder.setClient(domainClientDetails);
			
			OrderItem domainOrderItem = new OrderItem();
			domainOrderItem.setName(rs.getString("items"));
			List<OrderItem> domainItemsList = new ArrayList();
			domainItemsList.add(domainOrderItem);
			domainOrder.setItems(domainItemsList);
			
			if(domainOrder.getId()==1 
				|| domainOrder.getClient().getName().equals("?")
				|| domainOrder.getItems().get(0).equals("?")) {
				isTableInView = true;
			}
			
		}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			return isTableInView;

		}
	}
}
