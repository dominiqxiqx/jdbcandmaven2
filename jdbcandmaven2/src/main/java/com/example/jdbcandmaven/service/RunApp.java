package com.example.jdbcandmaven.service;

public class RunApp {

	public static void main(String[] args) {
		CreateTableOrder createTableOrder = new CreateTableOrder();
		createTableOrder.createTable();
		
		AddNewOrder addNewOrder = new AddNewOrder();
		addNewOrder.newOrder();
		
		ViewOrders viewOrders = new ViewOrders();
		viewOrders.viewOrders();
		
		DeleteOrder deleteOrder = new DeleteOrder();
		deleteOrder.deleteOrder();

	}

}
